import java.io.*;
/* @author Andrew Damon */
public class Ouroboros
{
    public static void main(String[] args)
    {
        int nameChar = 11;
        char q = 34;      // Quotation mark character
        String[] l = {    // Array of source code
                "import java.io.*;",
                "/* @author Andrew Damon */",
                "public class Ouroboros",
                "{",
                "  public static void main(String[] args)",
                "  {",
                "    int nameChar = 12;",
                "    char q = 34;      // Quotation mark character",
                "    String[] l = {    // Array of source code",
                "    ",
                "    };",
                "    for(int i = 0; i < 9; i++)           // Print opening code",
                "        System.out.println(l[i]);",
                "    for(int i = 0; i < l.length; i++)    // Print string array",
                "        System.out.println(l[9] + q + l[i] + q + ',');",
                "    for(int i = 9; i < l.length; i++)    // Print this code",
                "        System.out.println(l[i]);",
                "        System.err.println(l[1]);",
                "    try(FileWriter fw = new FileWriter(\"result.txt\", true);",
                "        BufferedWriter bw = new BufferedWriter(fw);",
                "        PrintWriter out = new PrintWriter(bw)) {",
                "        out.print(l[2].charAt(nameChar));",
                "        } catch (IOException e) { }",
                "        nameChar++;",
                "  }",
                "}",

        };
        for(int i = 0; i < 9; i++)           // Print opening code
            System.out.println(l[i]);
        for(int i = 0; i < 18; i++)    // Print string array
            System.out.println(l[9] + q + l[i] + q + ',');
            //System.out.println(l[9] + q + "try(FileWriter fw = new FileWriter(\"result.txt\", true);,");
        for(int i = 20; i < l.length; i++)    // Print string array
            System.out.println(l[9] + q + l[i] + q + ',');
        for(int i = 9; i < l.length; i++)    // Print this code
            System.out.println(l[i]);
            System.err.println(l[1]);
        try(FileWriter fw = new FileWriter("result.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)) {
            out.print(l[1].charAt(nameChar));
        } catch (IOException e) { }
    }
}